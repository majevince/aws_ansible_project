
# Create vpc us-east-1
resource "aws_vpc" "vpc_master" {

  cidr_block           = "10.0.0.0/16"
  provider             = aws.region-master
  enable_dns_support   = true
  enable_dns_hostnames = true
  instance_tenancy     = "default"

  tags = {
    Name = "master_vpc"
  }
}


#create vpc us-west-1
resource "aws_vpc" "vpc_worker" {

  cidr_block           = "192.168.0.0/16"
  provider             = aws.region-worker
  enable_dns_support   = true
  enable_dns_hostnames = true
  instance_tenancy     = "default"

  tags = {
    Name = "worker_vpc"
  }
}

#initiate vpc peering

resource "aws_vpc_peering_connection" "us_east1-us_west1" {
  #peer_owner_id = var.peer_owner_id
  peer_vpc_id = aws_vpc.vpc_worker.id
  vpc_id      = aws_vpc.vpc_master.id
  provider    = aws.region-master
  peer_region = var.region-worker
  #auto_accept   = true

  tags = {
    Name = "us_east1-us_west1"
  }
}


# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer" {
  provider                  = aws.region-worker
  vpc_peering_connection_id = aws_vpc_peering_connection.us_east1-us_west1.id

  auto_accept = true

  tags = {
    Side = "Accepter"
  }
}


# create IGW for us-east-1
resource "aws_internet_gateway" "igw-us-east-1" {
  vpc_id   = aws_vpc.vpc_master.id
  provider = aws.region-master


  tags = {
    Name = "igw-us-east-1"
  }


}



# create IGW for us-west-1
resource "aws_internet_gateway" "igw-us-west-1" {
  vpc_id   = aws_vpc.vpc_worker.id
  provider = aws.region-worker


  tags = {
    Name = "igw-us-west-1"
  }


}



# create route table on us-east-1
resource "aws_route_table" "route_table_useast1" {
  vpc_id   = aws_vpc.vpc_master.id
  provider = aws.region-master


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-us-east-1.id
  }

  route {
    cidr_block                = "192.168.1.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.us_east1-us_west1.id
  }

  tags = {
    Name = "route_table_useast1"
  }
}



# create route table on us-west-1
resource "aws_route_table" "route_table_uswest1" {
  vpc_id   = aws_vpc.vpc_worker.id
  provider = aws.region-worker


  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-us-west-1.id
  }

  route {
    cidr_block                = "10.0.1.0/24"
    vpc_peering_connection_id = aws_vpc_peering_connection.us_east1-us_west1.id
  }

  tags = {
    Name = "route_table_uswest1"
  }
}


resource "aws_main_route_table_association" "set_default_master_route" {
  provider = aws.region-master
  vpc_id   = aws_vpc.vpc_master.id


  route_table_id = aws_route_table.route_table_useast1.id
}




resource "aws_main_route_table_association" "set_default_worker_route" {

  vpc_id   = aws_vpc.vpc_worker.id
  provider = aws.region-worker

  route_table_id = aws_route_table.route_table_uswest1.id
}

# Declare the data source availability zones
data "aws_availability_zones" "azs" {
  state    = "available"
  provider = aws.region-master
}


resource "aws_subnet" "subnet_1" {
  vpc_id            = aws_vpc.vpc_master.id
  provider          = aws.region-master
  cidr_block        = "10.0.1.0/24"
  availability_zone = element(data.aws_availability_zones.azs.names, 0)

  tags = {
    Name = "subnet_1"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id            = aws_vpc.vpc_master.id
  provider          = aws.region-master
  cidr_block        = "10.0.2.0/24"
  availability_zone = element(data.aws_availability_zones.azs.names, 1)

  tags = {
    Name = "subnet_2"
  }
}


resource "aws_subnet" "subnet_worker" {
  vpc_id     = aws_vpc.vpc_worker.id
  provider   = aws.region-worker
  cidr_block = "192.168.1.0/24"

  tags = {
    Name = "subnet_worker"
  }
}

resource "aws_security_group" "master-sg" {
  name        = "allow_ssh connection"
  description = "Allow TLS ssh traffic"
  vpc_id      = aws_vpc.vpc_master.id
  provider    = aws.region-master

  ingress {
    description = "allow traffics from ssh connection"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }
  ingress {
    description = "allow traffics from us-west2 on 192.168.1.0/24"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["192.168.1.0/24"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }



  ingress {
    description = "allow traffics from alb on port 8080"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  ingress {
    description = "allow traffics  for public IP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }



  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "web-ssh_connection"
  }
}


resource "aws_security_group" "master-lb" {
  name        = "Lb_connection"
  description = "Allow TLS ssh traffic"
  vpc_id      = aws_vpc.vpc_master.id
  provider    = aws.region-master

  ingress {
    description = "allow traffics from tls connection 443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }
  ingress {
    description = "allow traffics from alb on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }



  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "ALB_connection"
  }
}




resource "aws_security_group" "worker-sg" {
  name        = "allow_ssh"
  description = "Allow TLS ssh traffic"
  vpc_id      = aws_vpc.vpc_worker.id
  provider    = aws.region-worker

  ingress {
    description = "allow traffics from ssh connection on port 22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

  ingress {
    description = "allow traffics from alb on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }




  ingress {
    description = "allow traffics from us_east_1 on port 0"
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24"]
    # ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }



  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "ssh_connection_worker"
  }
}


