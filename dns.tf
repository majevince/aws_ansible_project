#update hosted zones
data "aws_route53_zone" "dns" {
  name     = var.dns-name
  provider = aws.region-master
}


resource "aws_route53_record" "master" {
  zone_id  = data.aws_route53_zone.dns.zone_id
  provider = aws.region-master
  name     = join("master", [data.aws_route53_zone.dns.name])
  type     = "A"

  alias {
    name                   = aws_lb.lb-master.dns_name
    zone_id                = aws_lb.lb-master.zone_id
    evaluate_target_health = true
  }
}




#create record in hosted zones

resource "aws_route53_record" "cert-validation" {
  provider = aws.region-master
  for_each = {
    for dvo in aws_acm_certificate.master-lb-http.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.dns.zone_id
}

