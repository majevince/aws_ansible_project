#generate certificate
resource "aws_acm_certificate" "master-lb-http" {
  domain_name       = join("master", [data.aws_route53_zone.dns.name])
  provider          = aws.region-master
  validation_method = "DNS"

  tags = {
    Name = "master-lb-http"
  }


}


#ACM certificate validation
resource "aws_acm_certificate_validation" "example" {
  certificate_arn         = aws_acm_certificate.master-lb-http.arn
  provider                = aws.region-master
  for_each                = aws_route53_record.cert-validation
  validation_record_fqdns = [aws_route53_record.cert-validation[each.key].fqdn]
}


#Create Application LB
resource "aws_lb" "lb-master" {
  name     = "application-lb"
  provider = aws.region-master
  #vpc_id             = aws_vpc.vpc_master.id
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.master-lb.id]
  subnets            = [aws_subnet.subnet_1.id, aws_subnet.subnet_2.id]


  tags = {
    Name = "application-lb"
  }
}



#create LB target group
resource "aws_lb_target_group" "app-target-lb" {
  name        = "app-target-lb"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.vpc_master.id
  provider    = aws.region-master
  health_check {
    enabled  = true
    interval = 10
    path     = "/"
    port     = 80
    protocol = "HTTP"
    matcher  = "200-299"

  }


  tags = {
    Name = "master_target_group"
  }
}


resource "aws_lb_listener" "master_listener-http" {
  load_balancer_arn = aws_lb.lb-master.arn
  provider          = aws.region-master
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.master-lb-http.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app-target-lb.arn
  }
}




#resource "aws_lb_target_group_attachment" "test" {
 # target_group_arn = aws_lb_target_group.app-target-lb.arn
 # provider         = aws.region-master
 # target_id        = aws_autoscaling_group.master-grp.id
 # port             = 80
#}




