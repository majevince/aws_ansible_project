#get Linux AMI
data "aws_ssm_parameter" "linux_master_ami" {
  provider = aws.region-master
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}


data "aws_ssm_parameter" "linux_worker_ami" {
  provider = aws.region-worker
  name     = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"
}

#create key pair for ssh login
resource "aws_key_pair" "master_key" {
  key_name   = "master-key"
  provider   = aws.region-master
  public_key = file("~/.ssh/id_rsa.pub")
}

resource "aws_key_pair" "worker_key" {
  key_name   = "worker-key"
  provider   = aws.region-worker
  public_key = file("~/.ssh/id_rsa.pub")
}


#create instance


resource "aws_instance" "worker-node" {
  ami                         = data.aws_ssm_parameter.linux_worker_ami.value
  provider                    = aws.region-worker
  key_name                    = aws_key_pair.worker_key.key_name
  instance_type               = var.instace-type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.worker-sg.id]
  subnet_id                   = aws_subnet.subnet_worker.id
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y install httpd && sudo systemctl start httpd",
      "echo '<h1><center>My Welcome to worker Node</center></h1>' > index.html",
      "sudo mv index.html /var/www/html/",
      "sudo yum install firewalld -y",
      "sudo systemctl enable firewalld --now",
      "sudo firewall-cmd --permanent --add-port={443/tcp,80/tcp}",
      "sudo firewall-cmd --reload"
    ]
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("~/.ssh/id_rsa")
      host        = self.public_ip
    }
  }
  tags = {
    Name = "worker-node1"
  }
}


#Create Auto scaling launch configuration
resource "aws_launch_configuration" "app_conf" {
  name_prefix                 = "master-prod"
  provider                    = aws.region-master
  image_id                    = data.aws_ssm_parameter.linux_master_ami.value
  associate_public_ip_address = true
  key_name                    = aws_key_pair.master_key.key_name
  security_groups             = ["sg-0322227bb2bfeae9a"]
  instance_type               = var.instace-type

  lifecycle {
    create_before_destroy = true
  }




}





#create auto scaling group

resource "aws_autoscaling_group" "master-grp" {
  name                      = "auto-scale-grp-test"
  provider                  = aws.region-master
  max_size                  = 10
  min_size                  = 5
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 7
  force_delete              = true
  target_group_arns         = [aws_lb_target_group.app-target-lb.id]
  launch_configuration      = aws_launch_configuration.app_conf.name
  vpc_zone_identifier       = [aws_subnet.subnet_1.id, aws_subnet.subnet_2.id]

  tag {
    key                 = "lorem"
    value               = "ipsum"
    propagate_at_launch = false
  }
}
