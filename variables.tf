variable "region-master" {
  type    = string
  default = "us-east-1"
}


variable "region-worker" {
  type    = string
  default = "us-west-2"
}


variable "external-ip" {
  type    = string
  default = "0.0.0.0/0"

}


variable "instace-type" {
  type    = string
  default = "t3.micro"
}

variable "profile" {
  type    = string
  default = "default"
}

variable "dns-name" {
  type    = string
  default = "cmcloudlab986.info"
}
